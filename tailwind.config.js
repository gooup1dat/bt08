/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
  ],
  theme: {
    extend: {
      colors : {
        'green-green-400': 'var(--Green-Green-400, #48BB78)',
        'gray-gray-700': 'var(--Gray-Gray-700, #2D3748)',
        'gray-gray-400': 'var(--Gray-Gray-400, #A0AEC0)',
        'second-gray': 'var(--second-text, #A0AEC0)',
      },
      borderRadius : {
        c8: '8px',
        c15: '15px',
      },

      gap: {
        "c-14": "14px",
      },
      backgroundColor : {
        "Very-Light-Grey": "#CDCDCD",
        "Gray-Gray-300" : "#CBD5E0",
      },
      fontFamily: {
        helvetica: 'Helvetica, Arial, sans-serif',
      },
    },
  },
  plugins: [],
}

